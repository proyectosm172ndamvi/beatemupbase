using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextGameOverController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Has llegado a la oleada " + GManager.GMInstance.NumOleadaActaul;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
