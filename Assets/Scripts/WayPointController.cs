using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointController : MonoBehaviour
{
    [SerializeField]
    private GameObject nextWayPoint;
    public delegate void _OnPlayerDetected(GameObject nextWP);
    public event _OnPlayerDetected OnPlayerDetected;
    private int layerObjetivo;
    // Start is called before the first frame update
    void Start()
    {
        layerObjetivo = LayerMask.NameToLayer("Enemy");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == layerObjetivo)
        {
            OnPlayerDetected?.Invoke(nextWayPoint);
        }
    }
}
