using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeController : MonoBehaviour
{
    [SerializeField]
    private EnemySO enemySO; //scriptable object de enemigo
    public EnemySO EnemySO { get => enemySO; set => enemySO = value; }
    private enum Mira { Der, Izq }; //enum que nos indica donde mira el enemigo
    private Mira m_Mira;// donde mira actualmente
    private enum SwitchMachineStates { NONE, INI, IDLE, CHASE, SHOOT, PUPA, PATROL }; //enum de los estados de la m�quina de estados
    private SwitchMachineStates m_CurrentState; //estado actual de la m�quina
    [SerializeField]
    private GameObject objetivo; //objetivo que persigue el enemy
    public GameObject Objetivo { get => objetivo; set => objetivo = value; }
    public PoolController PoolBalas { get => poolBalas; set => poolBalas = value; }
    [HideInInspector]
    public GameObject patrolObjective; //waypoint al que se dirige
    private bool ObjetivoARango;
    private int vida;
    private int cldwn;
    private float velocity;
    //private bool puedeAtacar;
    [SerializeField]
    PoolController poolBalas;
    [SerializeField]
    GameEvent eventoMuerteEnemy;
    [SerializeField]
    Animator m_Animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
        //print("estoy a rango : "+ObjetivoARango);
    }

    private void Awake()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected += perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected += disparar;
        if (patrolObjective != null) patrolObjective.GetComponent<WayPointController>().OnPlayerDetected += changePatrol;
        //poolBalas.Incialeitor(poolBalas.PoolSize);
        ObjetivoARango = false;
        ActualizarSO();
        ChangeState(SwitchMachineStates.INI);
    }
    private void OnEnable()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected += perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected += disparar;
        if (patrolObjective != null) patrolObjective.GetComponent<WayPointController>().OnPlayerDetected += changePatrol;
        poolBalas.Incialeitor(poolBalas.PoolSize);
        ObjetivoARango = false;
        ActualizarSO();
        ChangeState(SwitchMachineStates.INI);
    }

    private void OnDisable()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected -= perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected -= disparar;
        if(patrolObjective != null) patrolObjective.GetComponent<WayPointController>().OnPlayerDetected -= changePatrol;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected -= perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected -= disparar;
        patrolObjective.GetComponent<WayPointController>().OnPlayerDetected -= changePatrol;
        StopAllCoroutines();
    }
    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.INI:
                this.gameObject.GetComponentInChildren<AreaDetectAttaController>().enabled = false;
                this.gameObject.GetComponentInChildren<AreaDetectChaseController>().enabled = false;
                m_Animator.Play("AnimEneRangeIdle");
                break;
            case SwitchMachineStates.IDLE:
                //this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                if(gameObject.activeInHierarchy) StartCoroutine("descanso");
                m_Animator.Play("AnimEneRangeIdle");

                break;

            case SwitchMachineStates.CHASE:

                m_Animator.Play("AnimEneRangeIdle");

                break;

            case SwitchMachineStates.SHOOT:
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                if (gameObject.activeInHierarchy) StartCoroutine("dispararBala");
                m_Animator.Play("AnimEneRangeIdle");
                break;

            case SwitchMachineStates.PUPA:
                this.gameObject.GetComponent<AudioSource>().Play();
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                // m_HitboxInfo.Damage = m_Hit2Damage;
                m_Animator.Play("AnimEneRangePupa");

                break;

            case SwitchMachineStates.PATROL:
                m_Animator.Play("AnimEneRangeIdle");
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (gameObject.activeInHierarchy) StopCoroutine("descanso");
                break;

            case SwitchMachineStates.CHASE:

                break;

            case SwitchMachineStates.SHOOT:

                if (gameObject.activeInHierarchy) StopCoroutine("dispararBala");

                break;
            case SwitchMachineStates.PATROL:


                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        //print(m_CurrentState);
        switch (m_CurrentState)
        {
            case SwitchMachineStates.INI:
                Vector3 direcam = (Camera.main.transform.position - transform.position).normalized;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = direcam * velocity;
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                    m_Mira = Mira.Izq;
                }
                else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                {
                    this.gameObject.transform.eulerAngles = Vector3.zero;
                    m_Mira = Mira.Der;
                }
                break;
            case SwitchMachineStates.IDLE:
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity != Vector2.zero)
                    ChangeState(SwitchMachineStates.CHASE);
                if (ObjetivoARango)
                    ChangeState(SwitchMachineStates.SHOOT);
                break;
            case SwitchMachineStates.CHASE:
                if (ObjetivoARango)
                    ChangeState(SwitchMachineStates.SHOOT);
                if (objetivo != null) {
                    Vector3 dire = (objetivo.transform.position - transform.position).normalized;
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = dire * velocity;

                    if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                    {
                        this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                        m_Mira = Mira.Izq;
                    }
                    else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                    {
                        this.gameObject.transform.eulerAngles = Vector3.zero;
                        m_Mira = Mira.Der;
                    }
                }

                break;
            case SwitchMachineStates.SHOOT:
                
                break;

            case SwitchMachineStates.PUPA:
                //ChangeState(SwitchMachineStates.IDLE);
                break;

            case SwitchMachineStates.PATROL:
                
                Vector3 dire2 = (patrolObjective.transform.position - transform.position).normalized;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = dire2 * velocity;
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                    m_Mira = Mira.Izq;
                }
                else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                {
                    this.gameObject.transform.eulerAngles = Vector3.zero;
                    m_Mira = Mira.Der;
                }
                
                break;

            default:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 12 && m_CurrentState == SwitchMachineStates.INI)
        {
            this.gameObject.GetComponentInChildren<AreaDetectAttaController>().enabled = true;
            this.gameObject.GetComponentInChildren<AreaDetectChaseController>().enabled = true;
            ChangeState(SwitchMachineStates.IDLE);
        }
        //Debug.Log(collision.gameObject);
        if (collision.gameObject.layer == 8)
        {
            //Debug.Log(string.Format("I've been hit by {0} and it did {1} damage to me",
            //    collision.name,
            //    collision.gameObject.GetComponent<HitboxInformer>().Damage));
            Damage(collision.gameObject.GetComponent<HitboxInformer>().Damage);
        }
    }

    private void Damage(int damageTaken)
    {
        vida -= damageTaken;
        //print(vida + "es la vida que me queda");
        if (vida <= 0)
        {
            eventoMuerteEnemy.Raise();
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            this.gameObject.SetActive(false);
        }
        else ChangeState(SwitchMachineStates.PUPA);
    }

    
    public void perseguir(bool activo)
    {
        if (activo) ChangeState(SwitchMachineStates.CHASE);
        else ChangeState(SwitchMachineStates.IDLE);
    }

    public void disparar(bool onRange) {
        if (onRange)
        {
            ChangeState(SwitchMachineStates.SHOOT);
            ObjetivoARango = true;
        }
        else
        {
            ObjetivoARango = false;
            perseguir(true);
        }
    }

    public void changePatrol(GameObject nWP)
    {
        patrolObjective = nWP;
        patrolObjective.GetComponent<WayPointController>().OnPlayerDetected += changePatrol;
    }

    public IEnumerator descanso() {
        yield return new WaitForSeconds(2);
        ChangeState(SwitchMachineStates.PATROL);
    }

    public IEnumerator dispararBala() {
        while (true)
        {
            GameObject bala = poolBalas.Pium();
            bala.GetComponent<BalaController>().objetivo = objetivo;
            bala.transform.position = this.transform.position;
            bala.GetComponentInParent<BalaController>().pium();
            yield return new WaitForSeconds(cldwn);
        }
    }

    public void ActualizarSO()
    {
        velocity = enemySO.MovVelocity;
        vida = enemySO.vida;
        cldwn = enemySO.cooldwn;
    }

    public void EndPupa()
    {
        //print("he entrado en ENDPupa");
        ChangeState(SwitchMachineStates.IDLE);
    }
}
