using System.Collections;
using UnityEngine;

public class EnemyMeleeController : MonoBehaviour
{
    [SerializeField]
    private EnemySO enemySO; //scriptable object de enemigo
    public EnemySO EnemySO { get => enemySO; set => enemySO = value; }
    private enum Mira { Der, Izq }; //enum que nos indica donde mira el enemigo
    private Mira m_Mira;// donde mira actualmente
    private enum SwitchMachineStates { NONE, INI, IDLE, WALK, HIT1, PUPA }; //enum de los estados de la m�quina de estados
    private SwitchMachineStates m_CurrentState; //estado actual de la m�quina
    [SerializeField]
    private GameObject objetivo; //objetivo que persigue el enemy
    public GameObject Objetivo { get => objetivo; set => objetivo = value; }
    private bool ObjetivoARango;
    private float velocity;
    private int vida;
    private int cldwn;
    private bool puedeAtacar;
    private bool puedeMoverse;
    [SerializeField]
    GameObject HitboxAtta;
    [SerializeField]
    Animator m_Animator;
    [SerializeField]
    GameEvent eventoMuerteEnemy;

    private void OnEnable()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected += perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected += atacar;
        ObjetivoARango = false;
        ActualizarSO();
        ChangeState(SwitchMachineStates.INI);
    }
    private void OnDisable()
    {
        this.gameObject.GetComponentInChildren<AreaDetectChaseController>().OnPlayerDetected -= perseguir;
        this.gameObject.GetComponentInChildren<AreaDetectAttaController>().OnPlayerDetected -= atacar;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.INI:
                this.gameObject.GetComponentInChildren<AreaDetectAttaController>().enabled = false;
                this.gameObject.GetComponentInChildren<AreaDetectChaseController>().enabled = false;
                m_Animator.Play("AnimEneMWalk");
                break;
            case SwitchMachineStates.IDLE:
                //this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                m_Animator.Play("AnimEneMIdle");

                break;

            case SwitchMachineStates.WALK:

                m_Animator.Play("AnimEneMWalk");

                break;

            case SwitchMachineStates.HIT1:

                //m_ComboAvailable = false;
                HitboxAtta.SetActive(true);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                m_Animator.Play("AnimEneMAtta");
                break;

            case SwitchMachineStates.PUPA:
                this.gameObject.GetComponent<AudioSource>().Play();
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                // m_HitboxInfo.Damage = m_Hit2Damage;
                m_Animator.Play("AnimEneMPupa");

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:

                //if (gameObject.activeInHierarchy) StopCoroutine("cooldown");

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        //print(m_CurrentState);
        //print(ObjetivoARango+"  "+puedeAtacar+"  "+puedeMoverse);
        switch (m_CurrentState)
        {          
            case SwitchMachineStates.INI:
                Vector3 direcam = (Camera.main.transform.position - transform.position).normalized;
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                    m_Mira = Mira.Izq;
                }
                else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                {
                    this.gameObject.transform.eulerAngles = Vector3.zero;
                    m_Mira = Mira.Der;
                }
                this.gameObject.GetComponent<Rigidbody2D>().velocity = direcam * velocity;
                break;

            case SwitchMachineStates.IDLE:                
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                if (ObjetivoARango && puedeAtacar)
                    ChangeState(SwitchMachineStates.HIT1);
                else if (puedeMoverse && puedeAtacar) {
                    ChangeState(SwitchMachineStates.WALK);
                }
                //else if(ObjetivoARango) //no est� entrando pq Objetivo a Rango mira si est� a rango de ataque no de persecucion
                //    ChangeState(SwitchMachineStates.WALK);
                break;
            case SwitchMachineStates.WALK:

                if (ObjetivoARango && puedeAtacar)
                    ChangeState(SwitchMachineStates.HIT1);
                //transform.position = Vector2.MoveTowards(transform.position, objetivo.transform.position, enemySO.MovVelocity * Time.deltaTime);
                if (objetivo != null)
                {
                    Vector3 dire = (objetivo.transform.position - transform.position).normalized;
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = dire * velocity;

                    if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                    {
                        this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                        m_Mira = Mira.Izq;
                    }
                    else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                    {
                        this.gameObject.transform.eulerAngles = Vector3.zero;
                        m_Mira = Mira.Der;
                    }
                }
                else
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }

                break;
            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.PUPA:

                break;

            default:
                break;
        }
    }

    public void perseguir(bool activo)
    {
        if (activo)
        {
            puedeMoverse = true;
            ChangeState(SwitchMachineStates.WALK);
        }
        else
        {
            puedeMoverse= false;
            ChangeState(SwitchMachineStates.IDLE);
        }
    }

    public void atacar(bool onRange)
    {
        if (onRange)
        {
            ObjetivoARango = true;
            puedeMoverse = false;
            ChangeState(SwitchMachineStates.HIT1);            
        }
        else
        {
            puedeMoverse = true;
            ObjetivoARango = false;
            ChangeState(SwitchMachineStates.WALK);
        }
    }

    public void EndHit()
    {
        //print("he entrado en ENDHIT");
        HitboxAtta.SetActive(false);
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void EndPupa() {
        //print("he entrado en ENDPupa");
        HitboxAtta.SetActive(false);
        if(puedeMoverse) ChangeState(SwitchMachineStates.WALK);
        else ChangeState(SwitchMachineStates.IDLE);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 12 && m_CurrentState == SwitchMachineStates.INI){
            this.gameObject.GetComponentInChildren<AreaDetectAttaController>().enabled = true;
            this.gameObject.GetComponentInChildren<AreaDetectChaseController>().enabled = true;
            ChangeState(SwitchMachineStates.IDLE);
        }
        //Debug.Log(collision.gameObject);
        if (collision.gameObject.layer == 8)
        {
            //Debug.Log(string.Format("I've been hit by {0} and it did {1} damage to me",
               // collision.name,
              //  collision.gameObject.GetComponent<HitboxInformer>().Damage));
            Damage(collision.gameObject.GetComponent<HitboxInformer>().Damage);
        }
    }

    private void Damage(int damageTaken)
    {
        vida -= damageTaken;
        //print(vida + "es la vida que me queda");
        if (vida <= 0)
        {
            eventoMuerteEnemy.Raise();
            this.gameObject.SetActive(false);
        }
        else ChangeState(SwitchMachineStates.PUPA);
    }

    public IEnumerator cooldown()
    {
        puedeAtacar = false;
        //print("no puc");
        yield return new WaitForSeconds(cldwn);
        puedeAtacar = true;
        //print("si puc");


    }

    public void ActualizarSO()
    {
        velocity = enemySO.MovVelocity;
        vida = enemySO.vida;
        cldwn = enemySO.cooldwn;
    }
}
