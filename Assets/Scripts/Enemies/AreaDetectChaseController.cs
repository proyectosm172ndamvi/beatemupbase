using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDetectChaseController : MonoBehaviour
{   
    public delegate void _OnPlayerDetected(bool activo);
    public event _OnPlayerDetected OnPlayerDetected;
    private int layerObjetivo;
    // Start is called before the first frame update
    void Start()
    {
        layerObjetivo = LayerMask.NameToLayer("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer ==layerObjetivo) {
            OnPlayerDetected?.Invoke(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == layerObjetivo)
        {
            OnPlayerDetected?.Invoke(false);
        }
    }
}
