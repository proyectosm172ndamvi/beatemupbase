using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemySO : ScriptableObject {

    public int vida; //vida del enemigo
    public float MovVelocity; // multipicador de velocidad de movimiento del enemigo
    //public LayerMask layerObjetivo; //layer a la que pertenece el objetivo al cual atacan;
    public int cooldwn;
}
