using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolController : MonoBehaviour
{
    [SerializeField]
    private GameObject proyectilPF;
    //[SerializeField]
    //private GameObject enemyMeleePF;
    //[SerializeField]
    //private GameObject enemyRangePF;
    [SerializeField]
    private List<GameObject> proyectilList;
    //[SerializeField]
    //private List<GameObject> enemiesList;
    [SerializeField]
    private int poolSize;

    //Singleton
    //private static PoolController instance;
    //public static PoolController Instance { get { return instance; } }

    public int PoolSize { get => poolSize; set => poolSize = value; }

    //private void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    else
    //    {
    //        Destroy(gameObject);
    //    }
    //}

    void OnEnable()
    {
        Incialeitor(PoolSize);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void AddBalas(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject proyectil = Instantiate(proyectilPF);
            proyectil.SetActive(false);
            proyectilList.Add(proyectil);
            proyectil.transform.parent = this.gameObject.transform;
        }
    }

    public GameObject Pium()
    {

        for (int i = 0; i < proyectilList.Count; i++)
        {
            if (!proyectilList[i].activeSelf)
            {            
                proyectilList[i].SetActive(true);
                return proyectilList[i];
            }
        }
        AddBalas(1);
        proyectilList[proyectilList.Count - 1].SetActive(true);
        return proyectilList[proyectilList.Count - 1];
    }

    //private void AddEnemies(int amount)
    //{
    //    for (int i = 0; i < amount; i++)
    //    {
    //        GameObject enemy = Instantiate(enemyPF);
    //        enemy.SetActive(false);
    //        enemiesList.Add(enemy);
    //    }
    //}

    //public GameObject Summon()
    //{
    //    for (int i = 0; i < enemiesList.Count; i++)
    //    {
    //        if (!enemiesList[i].activeSelf)
    //        {
    //            enemiesList[i].SetActive(true);
    //            return enemiesList[i];
    //        }
    //    }
    //    AddEnemies(1);
    //    enemiesList[enemiesList.Count - 1].SetActive(true);
    //    return enemiesList[enemiesList.Count - 1];
    //}

    public void Incialeitor(int PoolSize)
    {
        proyectilList.Clear();
        //enemiesList.Clear();
        AddBalas(PoolSize);
        //AddEnemies(PoolSize);
    }

}
