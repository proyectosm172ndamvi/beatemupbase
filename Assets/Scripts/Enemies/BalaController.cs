using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaController : MonoBehaviour
{

    [SerializeField]
    private float velocity;
    public GameObject objetivo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pium()
    {
        Vector3 dire = (objetivo.transform.position - transform.position).normalized;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = dire * velocity;
    }
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 6 || collision.gameObject.layer == 3)
        {
            this.gameObject.SetActive(false);
        }
    }

}
