using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class OleadaSO : ScriptableObject
{
    public int PorcentajeEneMelee;
    public int NumberOleada;
    public int NumberMaxEnemies;
    public int NumberOfSOEnemyMeleeInList;
    public int NumberOfSOEnemyRangeInList;
}
