using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] ListaWayPoints;
    [SerializeField]
    private GameObject objetivo;
    [SerializeField]
    private GameObject poolEnemyMelee;
    [SerializeField]
    private GameObject poolEnemyRange;
    [SerializeField]
    private EnemySO[] listaSOEnemiesMelee;//lista de So con las variables de melee para las diferentes oleadas
    [SerializeField]
    private EnemySO[] listaSOEnemiesRange;//lista de So con las variables de range para las diferentes oleadas
    private int contEnemigos; // este contador bajar� en uno cada vez que un enemigo "muera", se avisara que debe bajar utilizando GameEvents
    private int puntero; // puntero de la lista de SOoleada
    [SerializeField]
    private OleadaSO[] listaOleadaSO;// lista de oleadas
    private int numMaxEnemies; //numero m�ximo de enemigos que vamos a spawnear
    private int porcentajeEneMelee; //porcentaje de enemigo melle que van a spawnear
    private int numOleada; //numero de la oleada en la que estamos, empezamos por la 0
    private int NumberOfSOEnemyMeleeInList; // es el n�mero en la lista de SOMelee que queremos que nos devuelva la Oleada Actual
    private int NumberOfSOEnemyRangeInList; // es el n�mero en la lista de SORange que queremos que nos devuelva la Oleada Actual
    [SerializeField]
    private GameEvent cambioOleada; // evento que se lanza al cambiar la oleada
    [SerializeField]
    PoolController poolBalas;

    // Start is called before the first frame update
    void Start()
    {
        puntero = 0;
        numMaxEnemies = listaOleadaSO[puntero].NumberMaxEnemies;
        numOleada = listaOleadaSO[puntero].NumberOleada;
        porcentajeEneMelee = listaOleadaSO[puntero].PorcentajeEneMelee;
        GManager.GMInstance.NumOleadaActaul = numOleada;
        StartCoroutine(spawnear());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator spawnear() {
        //print(numMaxEnemies);
        for (int i = 0; i < numMaxEnemies; i++)
        {
            //print("SPAWNEO UN NUEVO ENEMIGO");
            GameObject newEne;
            int r = Random.Range(1, 101);
            if (r <= porcentajeEneMelee)
            {
                newEne = poolEnemyMelee.GetComponent<EnemyPool>().Summon();
                newEne.GetComponent<EnemyMeleeController>().Objetivo = objetivo;
                newEne.GetComponent<EnemyMeleeController>().EnemySO = ActualizarSOEnemies(true);
                newEne.GetComponent<EnemyMeleeController>().ActualizarSO();      
            }
            else {
                int r2 = Random.Range(0, ListaWayPoints.Length - 1);
                newEne = poolEnemyRange.GetComponent<EnemyPool>().SummonRange(ListaWayPoints[r2], poolBalas);
                newEne.GetComponent<EnemyRangeController>().Objetivo = objetivo;                
                newEne.GetComponent<EnemyRangeController>().EnemySO = ActualizarSOEnemies(false);
                newEne.GetComponent<EnemyRangeController>().ActualizarSO();
            }
            //print("voy a poner la nueva posicion al enemigo nuevo");
            if (i % 2 == 0)
                newEne.transform.position = new Vector3(-12, Random.Range(-3, 3), 0);
            else
                newEne.transform.position = new Vector3(12, Random.Range(-3, 3), 0);
            contEnemigos++;
            yield return new WaitForSeconds(1.5f);
        }
    }

    private EnemySO ActualizarSOEnemies(bool Melee) {
        if (Melee)
        {            
            return listaSOEnemiesMelee[NumberOfSOEnemyMeleeInList];
        }
        else
        {
            return listaSOEnemiesRange[NumberOfSOEnemyRangeInList];
        }
    }

    public void EnemigoMuerto() { //este metodo se llama atrav�s de un GameEvent cuando muere un enemigo
        //print("SALTA EL EVENTO");
        contEnemigos--;
        if(contEnemigos ==0)
            cambiarOleada();
    }

    private void cambiarOleada()
    {
        if (puntero < listaOleadaSO.Length-1)
        {
            puntero++;
            numMaxEnemies = listaOleadaSO[puntero].NumberMaxEnemies;
            numOleada = listaOleadaSO[puntero].NumberOleada;
            GManager.GMInstance.NumOleadaActaul = numOleada;
            porcentajeEneMelee = listaOleadaSO[puntero].PorcentajeEneMelee;
            NumberOfSOEnemyMeleeInList = listaOleadaSO[puntero].NumberOfSOEnemyMeleeInList;
            NumberOfSOEnemyRangeInList = listaOleadaSO[puntero].NumberOfSOEnemyRangeInList;
            cambioOleada.Raise();
            StartCoroutine(spawnear());
        }
        else {
            GManager.GMInstance.SceneChanger("victory");
            //aqui cambiariamos a la escena de victoria, usando un gameManager
            //print("se ha acabado la �ltima oleada, ganaste");
        }
        
    }
}
