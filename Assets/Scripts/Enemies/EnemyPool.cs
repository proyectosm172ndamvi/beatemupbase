using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyPF;
    [SerializeField]
    private List<GameObject> enemiesList;
    [SerializeField]
    private int poolSize;

    public int PoolSize { get => poolSize; set => poolSize = value; }

    // Start is called before the first frame update
    void Start()
    {
        Incialeitor(PoolSize);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void AddEnemies(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject enemy = Instantiate(enemyPF);
            enemy.SetActive(false);
            enemiesList.Add(enemy);
        }
    }

    private void AddEnemiesRange(int amount, GameObject wp, PoolController pool)
    {
        for (int i = 0; i < amount; i++)
        {           
            GameObject enemy = Instantiate(enemyPF);
            enemy.GetComponent<EnemyRangeController>().patrolObjective = wp;
            enemy.GetComponent<EnemyRangeController>().PoolBalas = pool;
            enemy.SetActive(false);
            enemiesList.Add(enemy);
        }
    }

    public GameObject Summon()
    {
        for (int i = 0; i < enemiesList.Count; i++)
        {
            if (!enemiesList[i].activeSelf)
            {
                enemiesList[i].SetActive(true);
                return enemiesList[i];
            }
        }
        AddEnemies(1);
        enemiesList[enemiesList.Count - 1].SetActive(true);
        return enemiesList[enemiesList.Count - 1];
    }

    public GameObject SummonRange(GameObject wp, PoolController pool)
    {
        for (int i = 0; i < enemiesList.Count; i++)
        {
            if (!enemiesList[i].activeSelf)
            {
                enemiesList[i].GetComponent<EnemyRangeController>().patrolObjective = wp;
                enemiesList[i].GetComponent<EnemyRangeController>().PoolBalas = pool;
                enemiesList[i].SetActive(true);
                return enemiesList[i];
            }
        }        
        AddEnemiesRange(1, wp, pool);
        enemiesList[enemiesList.Count - 1].SetActive(true);
        return enemiesList[enemiesList.Count - 1];
    }

    private void Incialeitor(int PoolSize)
    {
        enemiesList.Clear();
        //AddEnemies(PoolSize);
    }

}
