using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaController : MonoBehaviour
{
    public Gradient gradient;
    public Image fill;
    private float maxHp;
    private float currentHp;
    private float lastHp;
    [SerializeField]
    private float lerpTime;    

    // Start is called before the first frame update
    void Start()
    {
        maxHp = GManager.GMInstance.MaxHP;
        currentHp = GManager.GMInstance.HP1;
        fill.fillAmount = currentHp/maxHp;
        fill.color = gradient.Evaluate(fill.fillAmount);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void perderVida()
    {
        StopAllCoroutines();
        //StopCoroutine("ActualitzarVida");
        lastHp = currentHp;
        currentHp = GManager.GMInstance.HP1;
        StartCoroutine(ActualitzarVida());
    }


    private IEnumerator ActualitzarVida()
    {
        float currentTime = 0f;
        while (currentTime <= lerpTime)
        {
            fill.fillAmount = Mathf.Lerp(lastHp / maxHp, currentHp / maxHp, currentTime / lerpTime);            
            fill.color = gradient.Evaluate(fill.fillAmount);
            yield return null;
            currentTime += Time.deltaTime;
        }

    }
}
