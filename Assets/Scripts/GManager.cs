using UnityEngine;
using UnityEngine.SceneManagement;

public class GManager : MonoBehaviour
{
    //Singleton
    private static GManager gmanager;
    public static GManager GMInstance { get { return gmanager; } }

    public int MaxHP { get => maxHP;}
    public int HP1 { get => HP; set => HP = value; }
    public int NumOleadaActaul { get => numOleadaActaul; set => numOleadaActaul = value; }

    private int maxHP;
    private int HP;
    [SerializeField]
    private PlayerSO SOPlayer;
    private int numOleadaActaul;
    [SerializeField]
    private AudioClip audioInGame;
    [SerializeField]
    private AudioClip audioInVictory;
    [SerializeField]
    private AudioClip audioInGameOver;

    private void Awake()
    {
        if (gmanager == null) { gmanager = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(gameObject);
        maxHP = SOPlayer.MaxVida;
        HP1 = SOPlayer.vida;
        this.gameObject.GetComponent<AudioSource>().clip = audioInGame;
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SceneChanger(string escena) { 
        switch (escena)
        {
            case "juego":
                maxHP = SOPlayer.MaxVida;
                HP1 = SOPlayer.vida;
                NumOleadaActaul = 0;
                this.gameObject.GetComponent<AudioSource>().clip = audioInGame;
                this.gameObject.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene("SampleScene");
                break;
            case "gameover":
                this.gameObject.GetComponent<AudioSource>().clip = audioInGameOver;
                this.gameObject.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene("GameOverScene");
                break;
            case "victory":
                this.gameObject.GetComponent<AudioSource>().clip = audioInVictory;
                this.gameObject.GetComponent<AudioSource>().Play();
                SceneManager.LoadScene("VictoryScene");
                break;
        }
    }
}
