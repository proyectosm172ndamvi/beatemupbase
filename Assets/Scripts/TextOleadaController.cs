using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextOleadaController : MonoBehaviour
{
    private string texto;
    // Start is called before the first frame update
    void Start()
    {
        ActualizarTexto();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActualizarTexto()
    {
        switch(GManager.GMInstance.NumOleadaActaul)
        {
            case 0:
                texto = "\t     Oleada-0\nDespierta! Ya estamos Jugando";
                break;
            case 1:
                texto = "\t     Oleada-1\nSe supone que esto es un reto";
                break;
            case 2:
                texto = "\t     Oleada-2\nLas tres mellizas be like >:(";
                break;
            case 3:
                texto = "\t     Oleada-3\nY volo sobre mi lazando rayos";
                break;
            case 4:
                texto = "\t     Oleada-4\nLa revolucion de las maquinas";
                break;
            case 5:
                texto = "\t     Oleada-5\nEl team Rocket marca blanca";
                break;
            case 6:
                texto = "\t     Oleada-6\nQuien activo el bullet hell?";
                break;
            case 7:
                texto = "\t     Oleada-7\nLos Power Rangers daltonicos";
                break;
            case 8:
                texto = "\t     Oleada-8\nAyudaaa! Me estan matandooo!";
                break;
        }
        this.gameObject.GetComponent<TextMeshProUGUI>().text = texto;
    }
}
