using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PLayerController : MonoBehaviour
{
    [SerializeField]
    private PlayerSO SOPlayer;
    [SerializeField]
    private InputActionAsset InputActionAsset;
    private InputActionAsset Input;
    private InputAction m_MovementAction;
    private int vida;
    private enum Mira { Der, Izq }; //enum que nos indica donde mira el personaje
    private Mira m_Mira;
    private enum SwitchMachineStates { NONE, IDLE, WALK, HITf1, HITf2, HITF1, HITF2, PUPA }; //enum de los estados de la m�quina de estados
    //hitf1 + hitF1 ---> hitf2         hitF1 + hitf1 ---> hitF2
    private SwitchMachineStates m_CurrentState; //estado actual de la m�quina
    [SerializeField]
    Animator m_Animator;
    private bool m_ComboAvailable = false;
    [SerializeField]
    GameObject HitboxAtta;
    [SerializeField]
    private int HitboxAttaf1Damage;
    [SerializeField]
    private int HitboxAttaf2Damage;
    [SerializeField]
    private int HitboxAttaF1Damage;
    [SerializeField]
    private int HitboxAttaF2Damage;
    private bool invincible;
    [SerializeField]
    private GameEvent playerGolpeado;

    private void Awake()
    {
        Input = Instantiate(InputActionAsset); //Instanciem el asset (pf)
        m_MovementAction = Input.FindActionMap("AMDefault").FindAction("Movement"); //posem a la acci� la acci� de moviment del nostre mapa
        Input.FindActionMap("AMDefault").FindAction("Attackflojo").performed += AttackE; // suscribimos la funcion AttackE a la action de AttackflojoE para que se ejecute cuando se realice la accion
        Input.FindActionMap("AMDefault").FindAction("AttackFUERTE").performed += AttackQ; // suscribimos la funcion AttackQ a la action de AttackflojoQ para que se ejecute cuando se realice la accion
        Input.FindActionMap("AMDefault").Enable(); //Activem el ActionMap
    }

    private void OnDestroy()
    {
        Input.FindActionMap("AMDefault").FindAction("Attackflojo").performed -= AttackE; // nos desuscribimos
        Input.FindActionMap("AMDefault").FindAction("AttackFUERTE").performed -= AttackQ; // nos desuscribimos
        Input.FindActionMap("AMDefault").Disable(); //Desactivem el ActionMap
    }

    // Start is called before the first frame update
    void Start()
    {
        vida = SOPlayer.MaxVida;
        InitState(SwitchMachineStates.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }


    //canviarem d'estat sempre mitjanant aquesta funci
    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                m_Animator.Play("AnimPlayerIdle");

                break;

            case SwitchMachineStates.WALK:

                m_Animator.Play("AnimPlayerWalk");

                break;

            case SwitchMachineStates.HITf1:

                m_ComboAvailable = false;
                HitboxAtta.GetComponent<HitboxInformer>().Damage = HitboxAttaf1Damage;
                HitboxAtta.SetActive(true);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                //m_HitboxInfo.Damage = m_Hit1Damage;
                m_Animator.Play("AnimPlayerAttaf1");

                break;

            case SwitchMachineStates.HITF1:

                // m_ComboAvailable = false;
                HitboxAtta.GetComponent<HitboxInformer>().Damage = HitboxAttaF1Damage;
                HitboxAtta.SetActive(true);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                // m_HitboxInfo.Damage = m_Hit2Damage;
                m_Animator.Play("AnimPlayerAttaF1 1");

                break;

            case SwitchMachineStates.HITf2:

                m_ComboAvailable = false;
                HitboxAtta.GetComponent<HitboxInformer>().Damage = HitboxAttaf2Damage;
                HitboxAtta.SetActive(true);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                //m_HitboxInfo.Damage = m_Hit1Damage;
                m_Animator.Play("AnimPlayerAttaf2");

                break;

            case SwitchMachineStates.HITF2:

                // m_ComboAvailable = false;
                HitboxAtta.GetComponent<HitboxInformer>().Damage = HitboxAttaF2Damage;
                HitboxAtta.SetActive(true);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                // m_HitboxInfo.Damage = m_Hit2Damage;
                m_Animator.Play("AnimPlayerAttaF2 1");

                break;
            case SwitchMachineStates.PUPA:
                this.gameObject.GetComponent<AudioSource>().Play();
                m_Animator.Play("AnimPlayerPupa");
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HITf1:

                m_ComboAvailable = false;

                break;

            case SwitchMachineStates.HITf2:

                m_ComboAvailable = false;

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().x != 0 || m_MovementAction.ReadValue<Vector2>().y != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:
                this.gameObject.GetComponent<Rigidbody2D>().velocity = m_MovementAction.ReadValue<Vector2>() * SOPlayer.MovVelocity;
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0 && m_Mira == Mira.Der)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                    m_Mira = Mira.Izq;
                }
                else if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x > 0 && m_Mira == Mira.Izq)
                {
                    this.gameObject.transform.eulerAngles = Vector3.zero;
                    m_Mira = Mira.Der;
                }
                if (this.gameObject.GetComponent<Rigidbody2D>().velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;
            case SwitchMachineStates.HITf1:

                break;

            case SwitchMachineStates.HITF1:

                break;
            case SwitchMachineStates.HITf2:

                break;

            case SwitchMachineStates.HITF2:

                break;
            case SwitchMachineStates.PUPA:

                break;

            default:
                break;
        }
    }

    //AttackE equivle a los ataques q se lanzan cuando pulsamos la E
    private void AttackE(InputAction.CallbackContext actionContext)
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HITf1);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HITf1);

                break;

            case SwitchMachineStates.HITf1:


                ChangeState(SwitchMachineStates.HITf1);

                //print("Ataque flojo"); //lo realiza la segunda vez que pulsamos el attack flojo
                break;
            case SwitchMachineStates.HITf2:

                if (m_ComboAvailable)
                    ChangeState(SwitchMachineStates.HITF2);
                else
                    ChangeState(SwitchMachineStates.HITf2);

                //print("Ataque flojo"); //lo realiza la segunda vez que pulsamos el attack flojo
                break;
            case SwitchMachineStates.PUPA:

                break;

            default:
                break;
        }

    }

    //AttackQ equivle a los ataques q se lanzan cuando pulsamos la Q
    private void AttackQ(InputAction.CallbackContext actionContext)
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HITf2);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HITf2);

                break;

            case SwitchMachineStates.HITf2:


                ChangeState(SwitchMachineStates.HITf2);

                //print("Ataque flojo"); //lo realiza la segunda vez que pulsamos el attack flojo
                break;
            case SwitchMachineStates.HITf1:

                if (m_ComboAvailable)
                    ChangeState(SwitchMachineStates.HITF1);
                else
                    ChangeState(SwitchMachineStates.HITf2);

                //print("Ataque flojo"); //lo realiza la segunda vez que pulsamos el attack flojo
                break;

            default:
                break;
        }

    }

    public void InitComboWindow()
    {
        m_ComboAvailable = true;
    }

    public void EndComboWindow()
    {
        m_ComboAvailable = false;
    }

    public void EndHit()
    {
        invincible = false;
        HitboxAtta.SetActive(false);
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void TakesDamage() { 
        invincible = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 && !invincible) {
            Damage(collision.gameObject.GetComponent<HitboxInformer>().Damage);
        }
    }

    private void Damage(int damageTaken)
    {
        //print(vida);
        vida -= damageTaken;
        GManager.GMInstance.HP1 = vida;
        playerGolpeado.Raise();
        if (vida <= 0)
        {
            Destroy(this.gameObject);
            GManager.GMInstance.SceneChanger("gameover");
            //aqui usaremos el gameManager para cambiar a la escena de gameOver
        }
        else ChangeState(SwitchMachineStates.PUPA);
    }
}
