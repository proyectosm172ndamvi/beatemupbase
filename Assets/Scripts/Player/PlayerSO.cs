using UnityEngine;

[CreateAssetMenu]
public class PlayerSO : ScriptableObject
{

    public int vida; //vida del personaje
    public float MovVelocity; // multipicador de velocidad de movimiento de personaje
    public int MaxVida; //vida max del personaje
}
